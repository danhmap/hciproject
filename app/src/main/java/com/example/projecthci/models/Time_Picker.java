package com.example.projecthci.models;

public class Time_Picker {
    String time;
    Boolean isChecked;

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }

    public Time_Picker(String time, Boolean isChecked) {
        this.time = time;
        this.isChecked = isChecked;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Time_Picker(String time) {
        this.time = time;
    }
}
