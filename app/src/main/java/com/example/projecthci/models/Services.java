package com.example.projecthci.models;

public class Services {
    private int id;
    private String NameService;
    private String PriceService;
    private String mVote;
    private String mSale;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameService() {
        return NameService;
    }

    public void setNameService(String nameService) {
        NameService = nameService;
    }

    public String getPriceService() {
        return PriceService;
    }

    public void setPriceService(String priceService) {
        PriceService = priceService;
    }

    public String getmVote() {
        return mVote;
    }

    public void setmVote(String mVote) {
        this.mVote = mVote;
    }

    public String getmSale() {
        return mSale;
    }

    public void setmSale(String mSale) {
        this.mSale = mSale;
    }

    public Services(int id, String nameService, String priceService, String mVote, String mSale) {
        this.id = id;
        NameService = nameService;
        PriceService = priceService;
        this.mVote = mVote;
        this.mSale = mSale;
    }
}
