package com.example.projecthci.models;

public class SpaItem {
    private String spaName;
    private int spaImage;

    public SpaItem(String spaName,int spaImage){
        this.spaName=spaName;
        this.spaImage=spaImage;
    }
    public String getSpaName(){
        return spaName;
    }
    public int getSpaImage(){
        return spaImage;
    }
}
