package com.example.projecthci.models;

public class ServiceCat {
    private int id;
    private String NameService;
    private String PriceService;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameSer() {
        return NameService;
    }

    public void setNameSer(String nameSer) {
        this.NameService = nameSer;
    }

    public String getPriceSer() {
        return PriceService;
    }

    public void setPriceSer(String priceSer) {
        this.PriceService = priceSer;
    }

    public ServiceCat(int id, String nameSer, String priceSer) {
        this.id = id;
        this.NameService = nameSer;
        this.PriceService = priceSer;
    }
}
