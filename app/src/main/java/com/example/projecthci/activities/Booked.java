package com.example.projecthci.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.projecthci.R;

public class Booked extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout linear1 , linear2 , linear3;
    private Button mbtn_can1 , mbtn_can2 , mbtn_can3, mbtn_done;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booked);
        linear1 = (LinearLayout) findViewById(R.id.liner1);
        linear2 = (LinearLayout) findViewById(R.id.liner2);
        linear3 = (LinearLayout) findViewById(R.id.liner3);
        mbtn_can1 = (Button) findViewById(R.id.btn_Cancel);
        mbtn_can2 = (Button) findViewById(R.id.btn_Cancel2);
        mbtn_can3 = (Button) findViewById(R.id.btn_Cancel3);
        mbtn_done = (Button) findViewById(R.id.btn_done);
        mbtn_can1.setOnClickListener(this);
        mbtn_can2.setOnClickListener(this);
        mbtn_can3.setOnClickListener(this);
        mbtn_done.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_Cancel:
                linear1.setVisibility(View.GONE);
                break;
            case R.id.btn_Cancel2:
                linear2.setVisibility(View.GONE);
                break;
            case R.id.btn_Cancel3:
                linear3.setVisibility(View.GONE);
                break;
            case R.id.btn_done:
                Intent intent = new Intent(Booked.this , HomeActivity.class);
                startActivity(intent);
                break;
        }
    }
}
