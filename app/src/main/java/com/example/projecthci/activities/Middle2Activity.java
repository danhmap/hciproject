package com.example.projecthci.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.example.projecthci.R;

public class Middle2Activity extends AppCompatActivity implements View.OnClickListener {
    private ScrollView scrollView;
    private Button mbtn_back;
    private LinearLayout linear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_middle2);
        scrollView=(ScrollView) findViewById(R.id.scroll_view);
        mbtn_back = (Button) findViewById(R.id.btn_back);
        linear = (LinearLayout) findViewById(R.id.liner);
        mbtn_back.setOnClickListener(this);
        scrollView.setOnClickListener(this);
        linear.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_back:
                Intent intent = new Intent(this,HomeActivity.class);
                startActivity(intent);
                break;
            case R.id.scroll_view:
                Intent intent2 = new Intent(this, MapBookingActivity.class);
                startActivity(intent2);
                break;
            case R.id.liner:
                Intent intent1 = new Intent(this,Booking1Activity.class);
                startActivity(intent1);
                break;
        }
    }
}
