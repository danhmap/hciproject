package com.example.projecthci.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.projecthci.R;

import java.util.ArrayList;
import java.util.Calendar;

public class MapBooking2Activity extends AppCompatActivity implements View.OnClickListener {
    private Button btn_select_time, mbtn_back, mbtn_next , mbtn_select_date;
    private TextView txt_show_time,txt_show_date;
    private String[] listitems;
    String time;
    private boolean[] checkedItems;
    ArrayList<Integer> mTimer = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_booking);
        listitems=getResources().getStringArray(R.array.time);
        checkedItems = new boolean[listitems.length];
        txt_show_time = (TextView) findViewById(R.id.txt_show_time);
        btn_select_time = (Button) findViewById(R.id.btn_time_pick);
        mbtn_select_date=(Button) findViewById(R.id.btn_date_pick);
        txt_show_date=(TextView) findViewById(R.id.txt_show_date1);
        mbtn_back = (Button) findViewById(R.id.btn_back);
        mbtn_next = (Button) findViewById(R.id.btn_next);

        mbtn_back.setOnClickListener(this);

        mbtn_next.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_time_pick:
                AlertDialog.Builder builder = new AlertDialog.Builder(MapBooking2Activity.this);
                builder.setTitle("Vui lòng chọn giờ đặt lịch ");
//                builder.setMultiChoiceItems(listitems, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
//                        if(isChecked){
//                            if(!mTimer.contains(which)){
//                                mTimer.add(which);
//                            }else{
//                                mTimer.remove(which);
//                            }
//                        }
//                    }
//                });
                builder.setSingleChoiceItems(listitems, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        txt_show_time.setText("Lịch của bạn đã chọn là "+listitems[which]);
                    }
                });
                builder.setCancelable(false);
                builder.show();
                break;
            case R.id.btn_back:
                Intent intent = new Intent(this , Booking4Activity.class);
                startActivity(intent);
                break;
            case R.id.btn_next:
                Intent intent1 = new Intent(this,DoneActivity.class);
                startActivity(intent1);
                break;
            case R.id.btn_date_pick:
                Calendar calendar = Calendar.getInstance();
                int YEAR = calendar.get(Calendar.YEAR);
                int MONTH = calendar.get(Calendar.MONTH);
                int DAY = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String datepicker = "Ngày đặt lịch của bạn là " + dayOfMonth+"/" + month +"/"+year;
                        txt_show_date.setText(datepicker);
                    }
                }, YEAR,MONTH, DAY);
                datePickerDialog.show();
                break;
        }
    }
}
