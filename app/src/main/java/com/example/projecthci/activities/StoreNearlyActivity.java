package com.example.projecthci.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projecthci.R;
import com.example.projecthci.adapter.TimeAdapter;
import com.example.projecthci.models.Time_Picker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class StoreNearlyActivity extends AppCompatActivity implements View.OnClickListener  {
    private Button mDatePicker;
    private TextView mtxt_date_show;
    private TimeAdapter adapter;
    private Button mbtn_back,mbtn_next;
    private View mView;
    private List<Time_Picker> time_pickerList;
    private RecyclerView mRecyclerView;
    private LinearLayout ln_next;
    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_nearly);

        mbtn_back = (Button) findViewById(R.id.btn_back);
        mbtn_next=(Button) findViewById(R.id.btn_next);

        ln_next = (LinearLayout) findViewById(R.id.liner_next);
        mbtn_next.setOnClickListener(this);
        initView();
        //initData();
    }
    private void initView(){
        mRecyclerView = findViewById(R.id.crv_timepicker);
        int numberOfColomns = 4;
        mRecyclerView.setLayoutManager(new GridLayoutManager(this,numberOfColomns));

        ln_next.setOnClickListener(this);
        mbtn_back.setOnClickListener(this);

    }
    private void initData(){
        time_pickerList = new ArrayList<>();
        time_pickerList.add(new Time_Picker("8:00"));
        time_pickerList.add(new Time_Picker("8:30"));
        time_pickerList.add(new Time_Picker("9:00"));
        time_pickerList.add(new Time_Picker("9:30"));
        time_pickerList.add(new Time_Picker("10:00"));
        time_pickerList.add(new Time_Picker("10:30"));
        time_pickerList.add(new Time_Picker("11:00"));
        time_pickerList.add(new Time_Picker("11:30"));
        time_pickerList.add(new Time_Picker("12:00"));
        time_pickerList.add(new Time_Picker("12:30"));
        time_pickerList.add(new Time_Picker("13:00"));
        time_pickerList.add(new Time_Picker("13:30"));
        time_pickerList.add(new Time_Picker("14:00"));
        time_pickerList.add(new Time_Picker("14:30"));
        time_pickerList.add(new Time_Picker("15:00"));
        time_pickerList.add(new Time_Picker("15:30"));
        time_pickerList.add(new Time_Picker("16:00"));
        time_pickerList.add(new Time_Picker("16:30"));
        time_pickerList.add(new Time_Picker("17:00"));
        time_pickerList.add(new Time_Picker("17:30"));
        update();
    }
    private void update(){
        if(adapter == null){
            adapter = new TimeAdapter(this,time_pickerList);
            mRecyclerView.setAdapter(adapter);
        }else{
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.liner_next:
                Intent intent = new Intent(StoreNearlyActivity.this,GPSGifActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_back:
                Intent intent1 = new Intent(this, Booking1Activity.class);
                startActivity(intent1);
                break;
            case R.id.btn_next:
                Intent intent2 = new Intent(this, MiddleActivity.class);
                startActivity(intent2);
                break;
        }
    }



}
