package com.example.projecthci.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.projecthci.R;

public class DoneBookingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done_booking);
    }
}
