package com.example.projecthci.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.projecthci.R;
import com.example.projecthci.fragment.SettingFragment;

public class SaleActivity extends AppCompatActivity {
    private Button mbtn_done;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale);
        mbtn_done = (Button) findViewById(R.id.btn_done);
        mbtn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SaleActivity.this , HomeActivity.class);
                startActivity(intent);
            }
        });
    }
}
