package com.example.projecthci.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.projecthci.R;
import com.example.projecthci.fragment.HomeFragment;

public class DetailServiceDog extends AppCompatActivity {
    private ImageView imageView;
    private TextView mtxt_des, mtxt_title, mtxt_price, mtxt_price1, mtxt_service, mtxt_infor;
    private ScrollView mScrollView;
    private Button mbtn_done , mbtn_next, mbtn_rating;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_service);
        imageView = (ImageView) findViewById(R.id.img_view_detail);
        mtxt_des = (TextView) findViewById(R.id.txt_des);
        mtxt_title = (TextView) findViewById(R.id.txt_title);
        mbtn_next = (Button) findViewById(R.id.btn_book);
        mScrollView = (ScrollView) findViewById(R.id.scroll_view);
        mbtn_done = (Button) findViewById(R.id.btn_done);
        mtxt_price = (TextView) findViewById(R.id.txt_price);
        mtxt_service = (TextView) findViewById(R.id.txt_spaservice);
        mtxt_infor = (TextView) findViewById(R.id.txt_infor);
//        mbtn_rating = (Button) findViewById(R.id.btn_rating);

//        mbtn_rating.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                final AlertDialog dialog = new AlertDialog.Builder(DetailServiceDog.this)
////                        .setTitle("Thành công")
////                        .setMessage("Cảm ơn bạn đã gửi đánh giá. Góp ý của bạn \n sẽ giúp hoàn thiện dịch vụ hơn mỗi ngày.")
////                        .setPositiveButton("Đã xong", null)
////                        .show();
////
////                Button positiveButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
////                positiveButton.setOnClickListener(new View.OnClickListener() {
////                    @Override
////                    public void onClick(View v) {
////                        dialog.dismiss();
////                    }
////                });
//
////                AlertDialog.Builder mBuilder = new AlertDialog.Builder(DetailServiceDog.this);
////                View mView = getLayoutInflater().inflate(R.layout.dialog_rating, null);
////                Button rating_success = (Button) findViewById(R.id.btn_ratingsuccess);
////
////                rating_success.setOnClickListener(new View.OnClickListener() {
////                    @Override
////                    public void onClick(View v) {
////
////                    }
////                });
////
////                mBuilder.setView(mView);
////                AlertDialog dialog = mBuilder.create();
////                dialog.show();
//            }
//        });

        mbtn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailServiceDog.this, HomeActivity.class);
                startActivity(intent);
            }
        });
        mbtn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailServiceDog.this, Booking1Activity.class);
                startActivity(intent);
            }
        });




        imageView.setImageResource(getIntent().getIntExtra("image_id",0));
        mtxt_des.setText(getIntent().getIntExtra("des_id" , 0));
        mtxt_title.setText(getIntent().getIntExtra("title_id", 0));
        mtxt_price.setText(getIntent().getIntExtra("price_id", 0));
        mtxt_service.setText(getIntent().getIntExtra("service_id", 0));
        mtxt_infor.setText(getIntent().getIntExtra("information_id", 0));
    }
}
