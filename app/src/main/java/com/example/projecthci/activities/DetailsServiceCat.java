package com.example.projecthci.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.projecthci.R;
import com.example.projecthci.fragment.HomeFragment;

public class DetailsServiceCat extends AppCompatActivity {
    private ImageView imageView;
    private TextView mtxt_title_cat, mtxt_des_cat,mtxt_des,mtxt_price;
    private ScrollView mScrollView;
    private Button mbtn_done,mbtn_book;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_service_cat);
        imageView = (ImageView) findViewById(R.id.img_view_detail_cat);
        mtxt_des = (TextView) findViewById(R.id.txt_des);
        mbtn_book = (Button) findViewById(R.id.btn_book);
        mtxt_des_cat = (TextView) findViewById(R.id.txt_des_cat);
        mtxt_title_cat = (TextView) findViewById(R.id.txt_title_cat);
        mScrollView = (ScrollView) findViewById(R.id.scroll_view_cat);
        mtxt_price = (TextView) findViewById(R.id.txt_price);
        mbtn_done = (Button) findViewById(R.id.btn_done);

        imageView.setImageResource(getIntent().getIntExtra("image_id",0));
        mbtn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailsServiceCat.this, HomeActivity.class);
                startActivity(intent);
            }
        });
        mbtn_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailsServiceCat.this,Booking1Activity.class);
                startActivity(intent);
            }
        });
        mtxt_des_cat.setText(getIntent().getIntExtra("des_id", 0));
        mtxt_title_cat.setText(getIntent().getIntExtra("title_id", 0));
        mtxt_des.setText(getIntent().getIntExtra("des_id" , 0));
        mtxt_price.setText(getIntent().getIntExtra("price_id", 0));
    }
}
