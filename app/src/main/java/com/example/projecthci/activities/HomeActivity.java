package com.example.projecthci.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.example.projecthci.R;
import com.example.projecthci.fragment.BookFragment;
import com.example.projecthci.fragment.HomeFragment;
import com.example.projecthci.fragment.SettingFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;


public class HomeActivity extends AppCompatActivity {
    private BottomNavigationView mBottomNavigationView;
    private FrameLayout mManFram;
    private HomeFragment mHomeFragment;
    private BookFragment mBookFragment;
    private SettingFragment mSettingFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Intent intent = getIntent();
        //Bundle extras = intent.getExtras();
        //int status = extras.getInt("admin",0);
        Anhxa();
        setFragment(mHomeFragment);
        mBottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.bar_home:
                            setFragment(mHomeFragment);
                        return true;
                    case R.id.bar_book:
                        setFragment(mBookFragment);
                        return true;
                    case R.id.bar_setting:
                        setFragment(mSettingFragment);
                        return true;
                        default:return false;
                }
            }
        });
    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_fram,fragment);
        fragmentTransaction.commit();
    }

    public void Anhxa(){
        mManFram = (FrameLayout) findViewById(R.id.main_fram);
        mBottomNavigationView = (BottomNavigationView) findViewById(R.id.main_nav);
        mHomeFragment=new HomeFragment();
        mBookFragment = new BookFragment();
        mSettingFragment = new SettingFragment();


    }

}
