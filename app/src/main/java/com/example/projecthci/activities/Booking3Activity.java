package com.example.projecthci.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.projecthci.MainActivity;
import com.example.projecthci.R;
import com.example.projecthci.adapter.TimePickAdapter;

import java.util.ArrayList;
import java.util.Calendar;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Booking3Activity extends AppCompatActivity implements View.OnClickListener, TimePickAdapter.ItemClickListener {
    private Button mService, mBack, mNext;
    private TextView txt_show_time, txt_show_date;
    private RecyclerView rcv_address;
    private  Dialog dialog;
    private TimePickAdapter adapter;

    private TextView mtxt_service, mtxt_date, mtxt_time;
    private String[] listitems;
    private String[] listitemTimes;
    private boolean[] checkedItems;
    private Button btn_select_time, mbtn_select_date;
    ArrayList<Integer> mSerItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        listitems = getResources().getStringArray(R.array.service_item);
        listitemTimes = getResources().getStringArray(R.array.time);
        checkedItems = new boolean[listitems.length];
        mService = (Button) findViewById(R.id.btn_service);
        mtxt_service = (TextView) findViewById(R.id.txt_service);
        mtxt_time = (TextView) findViewById(R.id.txt_time);
        btn_select_time = (Button) findViewById(R.id.btn_time_pick);
        mbtn_select_date = (Button) findViewById(R.id.btn_date_pick);
        mBack = (Button) findViewById(R.id.btn_back);
        txt_show_date = (TextView) findViewById(R.id.txt_show_date1);
        txt_show_time = (TextView) findViewById(R.id.txt_show_time);
        mNext = (Button) findViewById(R.id.btn_next);
        mService.setOnClickListener(this);

        mBack.setOnClickListener(this);
        mNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_service:
                AlertDialog.Builder builder = new AlertDialog.Builder(Booking3Activity.this);
                builder.setTitle("Vui lòng chọn dịch vụ ");
                builder.setMultiChoiceItems(listitems, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position, boolean isChecked) {
                        if (isChecked) {
                            if (!mSerItems.contains(position)) {
                                mSerItems.add(position);
                            } else {
                                mSerItems.remove(position);
                            }
                        }
                    }
                });
                builder.setCancelable(false);
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String item = "Dịch vụ của bạn là : " + "\n" + "-";
                        for (int i = 0; i < mSerItems.size(); i++) {
                            item = item + listitems[mSerItems.get(i)];
                            if (i != mSerItems.size() - 1) {
                                item = item + "\n" + "-";
                            }
                        }
                        mtxt_service.setText(item);
                    }
                });
                builder.show();
                break;
            case R.id.btn_back:
                Intent intent = new Intent(this, SearchActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_next:
                Intent intent1 = new Intent(this , GPSGifActivity.class);
                startActivity(intent1);
                break;

            case R.id.btn_time_pick:
                showDialog(Booking3Activity.this);
//                 builder = new AlertDialog.Builder(BookingActivity.this);
//                builder.setTitle("Vui lòng chọn giờ đặt lịch ");
////                builder.setMultiChoiceItems(listitems, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
////                    @Override
////                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
////                        if(isChecked){
////                            if(!mTimer.contains(which)){
////                                mTimer.add(which);
////                            }else{
////                                mTimer.remove(which);
////                            }
////                        }
////                    }
////                });
//                builder.setSingleChoiceItems(listitemTimes, 0, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                        txt_show_time.setText("Lịch của bạn đã chọn là "+listitemTimes[which]);
//                    }
//                });
//                builder.setCancelable(false);
//                builder.show();

                break;
            case R.id.btn_date_pick:
                Calendar calendar = Calendar.getInstance();
                int YEAR = calendar.get(Calendar.YEAR);
                int MONTH = calendar.get(Calendar.MONTH);
                int DAY = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        String datepicker = "Ngày đặt lịch của bạn là " + dayOfMonth + "/" + month + "/" + year;
                        txt_show_date.setText(datepicker);
                    }
                }, YEAR, MONTH, DAY);
                datePickerDialog.show();
                break;
        }
    }

    public void showDialog(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

         dialog = new Dialog(activity);
//
//
//        // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_recycleview_time);


        Button btndialog = (Button) dialog.findViewById(R.id.btndialog);

        btndialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });





        String[] data = {"8h30 \n Hết chỗ ", "8h40 \n Hết chỗ ", "8h45 \n Hết chỗ ", "9h00", "9h15", "9h20", "9h40", "9h45",
                "10h00 \n Hết chỗ ", "10h15 \n Hết chỗ ", "10h20", "10h30", "10h40",
                "10h45", "11h00 \n Hết chỗ", "11h15", "11h20", "11h30", "11h40", "11h45", "12h00",
                "12h15", "12h20 \n Hết chỗ ", "12h30", "12h40 \n Hết chỗ", "12h45", "13h00",
                "13h15", "13h20 \n Hết chỗ", "13h30", "13h40 \n Hết chỗ", "13h45 \n Hết chỗ", "14h00", "14h15", "14h20", "14h30 \n Hết chỗ", "14h40", "14h45",
                "15h00", "15h15", "15h20",
                "15h30", "15h40 \n Hết chỗ", "15h45 \n Hết chỗ", "16h00 \n Hết chỗ", "16h15 \n Hết chỗ", "16h20", "16h30", "16h40", "16h45", "17h00"
                , "17h15", "17h20 \n Hết chỗ ", "17h30", "17h40 \n Hết chỗ", "17h45", "18h00", "18h15", "18h20", "18h30", "18h40", "18h45",
                "19h00", "19h15", "19h20", "19h30", "19h40", "19h45", "20h00", "20h15", "20h20", "20h30", "20h40", "20h45"
        };


        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.rcv_timeSelect);
        int numberOfColumns = 6;
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns, RecyclerView.HORIZONTAL, false));
        adapter = new TimePickAdapter(this, data);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT);

    }

    @Override
    public void onItemClick(View view, final int position) {



        if (adapter.getItem(position).contains("Hết chỗ")) {
            return;

        } else {
            new SweetAlertDialog(Booking3Activity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Quý khách muốn đặt vào " + adapter.getItem(position) + "?")

                    .setConfirmText("Xác Nhận")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            txt_show_time.setText("Thời gian của bạn đã chọn là " + adapter.getItem(position));
                            dialog.dismiss();
                            sDialog.dismissWithAnimation();
                        }
                    })
                    .setCancelButton("Hủy", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                        }
                    })
                    .show();
        }

//        Log.i("TAG", "You clicked number " + adapter.getItem(position) + ", which is at cell position " + position);
    }

}
