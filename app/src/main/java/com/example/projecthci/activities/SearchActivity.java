package com.example.projecthci.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projecthci.R;
import com.example.projecthci.adapter.AutoCompleteSpaAdapter;
import com.example.projecthci.models.SpaItem;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {
    private List<SpaItem> spaItemList;
    private Button btn_back, btn_back1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        btn_back = (Button) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        btn_back1 = (Button) findViewById(R.id.btn_back1);
        btn_back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });
        fillSpaList();
        final AutoCompleteTextView editText = findViewById(R.id.txtSearch);

        AutoCompleteSpaAdapter adapter = new AutoCompleteSpaAdapter(this,
                spaItemList);
        editText.setAdapter(adapter);




        editText.setOnItemClickListener(

                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        LinearLayout ln = (LinearLayout) findViewById(R.id.notFound);
                        LinearLayout spaInfo = (LinearLayout) findViewById(R.id.infoSpa);
                        TextView spaName = (TextView) findViewById(R.id.nameSpa);
                        ImageView imgSpa = (ImageView) findViewById(R.id.imgSpa);
                        SpaItem spaItem = (SpaItem) parent.getItemAtPosition(position);

                        imgSpa.setImageResource(spaItem.getSpaImage());
                        spaName.setText(spaItem.getSpaName());

                        spaInfo.setVisibility(View.VISIBLE);
                        ln.setVisibility(View.GONE);
                        btn_back.setVisibility(View.VISIBLE);
                        btn_back1.setVisibility(View.GONE);
                        Button btnNext = (Button) findViewById(R.id.btnNext);
                        btnNext.setVisibility(View.VISIBLE);
                    }


                });


        Button btnNext = (Button) findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchActivity.this, Booking6Activity.class);
                startActivity(intent);
            }
        });

    }

    private void fillSpaList() {
        spaItemList = new ArrayList<>();
        spaItemList.add(new SpaItem("Spa Petq ", R.drawable.logo2));
        spaItemList.add(new SpaItem("Pet Markw ", R.drawable.logo2));
        spaItemList.add(new SpaItem("Pet Caree ", R.drawable.logo3));
        spaItemList.add(new SpaItem("Petr Cat andy Dogu Spai ", R.drawable.logo4));
        spaItemList.add(new SpaItem("Peto Phuong Spafgh ", R.drawable.logo4));
        spaItemList.add(new SpaItem("Adultj Petkl", R.drawable.logo2));
        spaItemList.add(new SpaItem("Buniozxc", R.drawable.logo1));
        spaItemList.add(new SpaItem("Sweetv Petnm", R.drawable.logo3));

    }
}
