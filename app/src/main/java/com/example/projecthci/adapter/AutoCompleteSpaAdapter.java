package com.example.projecthci.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.projecthci.R;
import com.example.projecthci.models.SpaItem;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteSpaAdapter extends ArrayAdapter<SpaItem> {
    private List<SpaItem> spaItemListFull;

    public AutoCompleteSpaAdapter(@NonNull Context context, @NonNull List<SpaItem> spaItemList) {
        super(context, 0, spaItemList);
        spaItemListFull = new ArrayList<>(spaItemList);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return spaFilter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spa_autocomplete_row, parent, false
            );

        }
        TextView textViewName = convertView.findViewById(R.id.txt_name_spa);
        ImageView imageViewSpa = convertView.findViewById(R.id.image_view_spa);

        SpaItem spaItem = getItem(position);
        if (spaItem != null) {
            textViewName.setText(spaItem.getSpaName());
            imageViewSpa.setImageResource(spaItem.getSpaImage());

        }
        return convertView;
    }

    private Filter spaFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            List<SpaItem> suggestions = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                suggestions.addAll(spaItemListFull);

            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (SpaItem item : spaItemListFull) {
                    if (item.getSpaName().toLowerCase().contains(filterPattern)) {
                        suggestions.add(item);

                    }
                    else {

                    }
                }
            }
            results.values = suggestions;
            results.count = suggestions.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((List) results.values);
            notifyDataSetChanged();

        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((SpaItem) resultValue).getSpaName();

        }
    };
}
