package com.example.projecthci.adapter;

import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.projecthci.fragment.ServiceBeakFragment;
import com.example.projecthci.fragment.ServiceTrimFragment;
import com.example.projecthci.fragment.ServiceShowerFragment;
import com.example.projecthci.fragment.ServiceLookFragment;


public class ServiceHomeFragmentAdapter extends FragmentStatePagerAdapter {
    private static int NUMBER_PAGE = 4;

    public ServiceHomeFragmentAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ServiceShowerFragment.newInstance();
            case 1:
                return ServiceBeakFragment.newInstance();
            case 2:
                return ServiceShowerFragment.newInstance();
            case 3:
                return ServiceBeakFragment.newInstance();
            default:
                return ServiceShowerFragment.newInstance();
        }
    }
    @Override
    public void restoreState(final Parcelable state, final ClassLoader loader) {
        try {
            super.restoreState(state, loader);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        return NUMBER_PAGE;
    }
}
