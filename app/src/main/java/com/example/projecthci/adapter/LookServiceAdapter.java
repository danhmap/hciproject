package com.example.projecthci.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projecthci.R;
import com.example.projecthci.activities.DetailServiceDog;
import com.example.projecthci.models.Services;

import java.util.List;

public class LookServiceAdapter extends RecyclerView.Adapter<LookServiceAdapter.ViewHolder> {

    private Context mContext;
    private List<Services> mServices;
    private int [] images = {R.drawable.logo2,R.drawable.logo4,R.drawable.logo1,R.drawable.logo3,R.drawable.banner_tiemphong,R.drawable.banner_taokieu};
    private int [] des = {R.string.desTamCho , R.string.des_catmong, R.string.desGiuThu, R.string.desChaiLong, R.string.desTiemPhong, R.string.desTaoKieuLong};
    private int[] title = {R.string.title_tamcho, R.string.title_catmong, R.string.title_giuthu, R.string.title_chailong, R.string.title_tiemphong, R.string.title_taokieulong};
    public LookServiceAdapter(Context mContext, int[] images, int[] des, int[] title) {
        this.mContext = mContext;
        this.images = images;
        this.des = des;
        this.title = title;
    }

    public LookServiceAdapter(int[] images){
        this.images = images;
    }

    public LookServiceAdapter(Context mContext, List<Services> mServices) {
        this.mContext = mContext;
        this.mServices = mServices;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater =  LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_service, parent, false);
        return new ViewHolder(view,mContext,images, des, title);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtname.setText(mServices.get(position).getNameService());
        holder.txtprice.setText(mServices.get(position).getPriceService());
        // holder.txtvote.setText(mServices.get(position).getmVote());
        int image_id = images[position];
        holder.imgSer.setImageResource(image_id);


    }

    @Override
    public int getItemCount() {
        return mServices.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtname, txtprice, txtvote;
        private ImageView imgSer;
        private Context mContext;
        private int[] images;
        private int[] des;
        private int[] title;
        public ViewHolder(@NonNull View itemView , Context mContext , int[] images, int[] des , int[] title) {

            super(itemView);
            imgSer = (ImageView) itemView.findViewById(R.id.img_service);
            txtname = (TextView) itemView.findViewById(R.id.txt_nameService);
            txtprice=(TextView) itemView.findViewById(R.id.txt_price_service);
            // txtvote=(TextView) itemView.findViewById(R.id.txt_vote);
            itemView.setOnClickListener(this);
            this.mContext = mContext;
            this.images = images;
            this.des = des;
            this.title = title;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, DetailServiceDog.class);
            intent.putExtra("image_id", images[getAdapterPosition()]);
            intent.putExtra("des_id", des[getAdapterPosition()]);
            intent.putExtra("title_id", title[getAdapterPosition()]);
            mContext.startActivity(intent);
        }
    }
}
