package com.example.projecthci.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projecthci.R;
import com.example.projecthci.activities.DetailServiceDog;
import com.example.projecthci.models.Services;

import java.util.List;

public class DogServiceAdapter extends RecyclerView.Adapter<DogServiceAdapter.ViewHolder> {

    private Context mContext;
    private List<Services> mServices;
    private int [] images = {R.drawable.logo2,R.drawable.logo4,R.drawable.logo1,R.drawable.logo3,R.drawable.banner_tiemphong,R.drawable.banner_taokieu};
    private int [] des = {R.string.price_giagoc1, R.string.price_giagoc2, R.string.price_giagoc3, R.string.price_giagoc4, R.string.price_giagoc5, R.string.price_giagoc6};
    private int[] title = {R.string.title_tamcho, R.string.title_catmong, R.string.title_giuthu, R.string.title_chailong, R.string.title_tiemphong, R.string.title_taokieulong};
    private int[] price =  {R.string.price_dichvu1, R.string.price_dichvu2, R.string.price_dichvu3, R.string.price_dichvu4, R.string.price_dichvu5, R.string.price_dichvu6};
    private int[] service = {R.string.service_tam, R.string.service_tam1, R.string.service_tam2, R.string.service_tam3, R.string.service_chichngua, R.string.service_giuthucung};
    private int[] information = {R.string.infor_1,R.string.infor_1,R.string.infor_1,R.string.infor_1,R.string.infor_1,R.string.infor_1};

    public DogServiceAdapter(Context mContext, int[] images, int[] des, int[] title, int[] price, int[] service, int[] information) {
        this.mContext = mContext;
        this.images = images;
        this.des = des;
        this.title = title;
        this.price = price;
        this.service = service;
        this.information = information;
    }

    public DogServiceAdapter(int[] images){
        this.images = images;
    }

    public DogServiceAdapter(Context mContext, List<Services> mServices) {
        this.mContext = mContext;
        this.mServices = mServices;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater =  LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_service, parent, false);
        return new ViewHolder(view,mContext,images, des, title, price, service, information);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtname.setText(mServices.get(position).getNameService());
        holder.txtprice.setText(mServices.get(position).getPriceService());
        //holder.txtvote.setText(mServices.get(position).getmVote());
        holder.txtsale.setText(mServices.get(position).getmSale());
        int image_id = images[position];
        holder.imgSer.setImageResource(image_id);


    }

    @Override
    public int getItemCount() {
        return mServices.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtname, txtprice, txtvote,txtsale;
        private ImageView imgSer;
        private Context mContext;
        private int[] images;
        private int[] des;
        private int[] title;
        private int[] price;
        private int[] service;
        private int[] information;
        public ViewHolder(@NonNull View itemView , Context mContext , int[] images, int[] des , int[] title, int[] price, int[] service, int[] information) {

            super(itemView);
            imgSer = (ImageView) itemView.findViewById(R.id.img_service);
            txtname = (TextView) itemView.findViewById(R.id.txt_nameService);
            txtprice=(TextView) itemView.findViewById(R.id.txt_price_service);
            //txtvote=(TextView) itemView.findViewById(R.id.txt_vote);
            txtsale=(TextView) itemView.findViewById(R.id.txt_sale);
            itemView.setOnClickListener(this);
            this.mContext = mContext;
            this.images = images;
            this.des = des;
            this.title = title;
            this.price = price;
            this.service = service;
            this.information = information;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, DetailServiceDog.class);
            intent.putExtra("image_id", images[getAdapterPosition()]);
            intent.putExtra("des_id", des[getAdapterPosition()]);
            intent.putExtra("title_id", title[getAdapterPosition()]);
            intent.putExtra("price_id", price[getAdapterPosition()]);
            intent.putExtra("service_id", service[getAdapterPosition()]);
            intent.putExtra("information_id", information[getAdapterPosition()]);
            mContext.startActivity(intent);
        }
    }

}
