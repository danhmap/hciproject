package com.example.projecthci.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projecthci.R;

public class TimePickAdapter extends RecyclerView.Adapter<TimePickAdapter.ViewHolder> {
    public String[] mData;
    public LayoutInflater mInflater;
    public ItemClickListener mClickListener;

    private int selected_position = -1;

    // data is passed into the constructor
    public TimePickAdapter(Context context, String[] data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_item_time, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each cell
    @Override
    public void onBindViewHolder(@NonNull  ViewHolder holder,  int position) {


//        if (){
//            holder.myTextView.setText(mData[position]);

//        }
//        else{
//
//        }
        holder.myTextView.setText(mData[position]);


        for (int i = 0; i <= getItemCount(); i++) {
            String content = holder.myTextView.getText().toString();
            if (holder.myTextView.getText().toString().contains("Hết chỗ")) {
                Spannable spannable = new SpannableString(content);
                spannable.setSpan(new ForegroundColorSpan(Color.GRAY), content.indexOf("Hết chỗ"),
                        content.indexOf("Hết chỗ") + "Hết chỗ".length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                holder.myTextView.setText(spannable);
            } else {

            }
        }

//        if (selected_position == position) {
//            holder.linearLayout.setBackgroundColor(Color.parseColor("#FF7ED3"));
//            holder.myTextView.setTextColor(Color.parseColor("#FFFFFF"));
//
//        } else {
//            holder.linearLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));
//            holder.myTextView.setTextColor(Color.parseColor("#FF7ED3"));
//        }
//
//
//        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (holder.myTextView.getText().toString().contains("Hết chỗ")) {
//                    return;
//                }
//
//                selected_position = position;
//                Log.d("phuong",String.valueOf(position));
//
//                notifyDataSetChanged();
//            }
//        });


    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mData.length;
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout linearLayout;
        TextView myTextView;

        ViewHolder(View itemView) {
            super(itemView);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.LnTime);
            myTextView = itemView.findViewById(R.id.info_text_time);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return mData[id];
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, final int position);
    }


}


