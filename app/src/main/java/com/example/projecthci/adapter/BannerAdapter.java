package com.example.projecthci.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.example.projecthci.R;

import java.util.List;

public class BannerAdapter extends PagerAdapter {
    private Context mContext;
    private List<Integer> mBanners;

    public BannerAdapter(Context mContext, List<Integer> mBanners) {
        this.mContext = mContext;
        this.mBanners = mBanners;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_banner, null);
        ImageView imageView = view.findViewById(R.id.img_banner);
        imageView.setImageResource(mBanners.get(position));
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object view) {
        container.removeView((View) view);
    }

    @Override
    public int getCount() {
        return mBanners != null ? mBanners.size() : 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return o == view;
    }
}
