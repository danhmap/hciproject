package com.example.projecthci.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projecthci.R;
import com.example.projecthci.activities.DetailsServiceCat;
import com.example.projecthci.models.Services;

import java.util.List;

public class CatServiceAdapter extends RecyclerView.Adapter<CatServiceAdapter.ViewHolder> {
    private Context mContext;
    private List<Services> mServices;
    private int [] images = {R.drawable.cat_service3,R.drawable.cat_service1,R.drawable.banner_tronggiumeo,R.drawable.cat_service4,R.drawable.banner_tiemphongmeo,R.drawable.banner_taohinhlongmeo};
    private int[] des = {R.string.desTamCho, R.string.des_catmong, R.string.desTrongMeo, R.string.desChaiLong, R.string.desTiemPhong, R.string.desTaoKieuLong};
    private int[] title = {R.string.title_tamcho, R.string.title_catmong, R.string.title_TrongMeo, R.string.title_chailong, R.string.title_tiemphong, R.string.title_taokieulong};
    private int[] price =  {R.string.price_dichvu1, R.string.price_dichvu2, R.string.price_dichvu3, R.string.price_dichvu4, R.string.price_dichvu5, R.string.price_dichvu6};
    public CatServiceAdapter(Context mContext, int[] images, int[] des, int[] title , int[] price) {
        this.mContext = mContext;
        this.images = images;
        this.des = des;
        this.title = title;
        this.price = price;
    }

    public CatServiceAdapter(int[] images){
        this.images = images;
    }

    public CatServiceAdapter(Context mContext, List<Services> mServices) {
        this.mContext = mContext;
        this.mServices = mServices;
    }

    @NonNull
    @Override
    public CatServiceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater =  LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_service_cat, parent, false);
        return new ViewHolder(view, mContext, images, des, title,price);
    }

    @Override
    public void onBindViewHolder(@NonNull CatServiceAdapter.ViewHolder holder, int position) {
        holder.txtnameSer.setText(mServices.get(position).getNameService());
        holder.txtpriSer.setText(mServices.get(position).getPriceService());

        int image_id = images[position];
        holder.imgSer.setImageResource(image_id);


    }

    @Override
    public int getItemCount() {
        return mServices.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtnameSer , txtpriSer;
        private ImageView imgSer;
        private Context mContext;
        private int[] images;
        private int[] des;
        private int[] title;
        private int[] price;
        public ViewHolder(@NonNull View itemView, Context mContext , int[] images, int[] des, int[] title, int[] price) {
            super(itemView);
            imgSer = (ImageView) itemView.findViewById(R.id.img_service);
            txtnameSer = (TextView) itemView.findViewById(R.id.txt_nameService);
            txtpriSer=(TextView) itemView.findViewById(R.id.txt_price_service);

            itemView.setOnClickListener(this);
            this.mContext = mContext;
            this.images = images;
            this.des = des;
            this.title = title;
            this.price = price;

        }
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, DetailsServiceCat.class);
            intent.putExtra("image_id", images[getAdapterPosition()]);
            intent.putExtra("des_id", des[getAdapterPosition()]);
            intent.putExtra("title_id", title[getAdapterPosition()]);
            intent.putExtra("price_id", price[getAdapterPosition()]);
            mContext.startActivity(intent);
        }
    }
}
