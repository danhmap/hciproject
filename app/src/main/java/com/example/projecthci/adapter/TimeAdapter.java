package com.example.projecthci.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projecthci.R;
import com.example.projecthci.models.Time_Picker;

import org.w3c.dom.Text;

import java.sql.Time;
import java.util.List;

public class TimeAdapter extends RecyclerView.Adapter<TimeAdapter.ViewHolder> {
    private Context mContext;
    private List<Time_Picker> mTime_Picker;
    private int selectedPos = RecyclerView.NO_POSITION;

    public TimeAdapter(Context mContext, List<Time_Picker> mTime_Picker) {
        this.mContext = mContext;
        this.mTime_Picker = mTime_Picker;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater =  LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_time,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.txt_time.setText(mTime_Picker.get(position).getTime());
    holder.itemView.setSelected(selectedPos==position);



    }

    @Override
    public int getItemCount() {
        return mTime_Picker.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txt_time;
        private TextView txt_show_time;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_time = (TextView) itemView.findViewById(R.id.txt_time);
            txt_show_time=(TextView) itemView.findViewById(R.id.txt_show_time);

        }


        @Override
        public void onClick(View v) {
            notifyItemChanged(selectedPos);
            selectedPos = getLayoutPosition();
            notifyItemChanged(selectedPos);
        }
    }

}
