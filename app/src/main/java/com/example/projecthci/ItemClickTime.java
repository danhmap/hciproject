package com.example.projecthci;

import android.view.View;

public interface ItemClickTime {
    void onClick(View view , int position);
}
