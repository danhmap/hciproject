package com.example.projecthci.fragment;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projecthci.R;
import com.example.projecthci.adapter.BeakServiceAdapter;
import com.example.projecthci.adapter.DogServiceAdapter;
import com.example.projecthci.models.Services;

import java.util.ArrayList;
import java.util.List;

public class ServiceBeakFragment extends Fragment {
    private View mView;
    private BeakServiceAdapter mBeakServiceAdapter;
    private List<Services> mServices;
    private RecyclerView mRecyclerView;
    private LinearLayout ln_menu;
    private TextView txt_title_service;

    public ServiceBeakFragment() {
        // Required empty public constructor
    }

    public static ServiceBeakFragment newInstance() {
        ServiceBeakFragment fragment = new ServiceBeakFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_service_trim, container, false);
        initView();
        initData();
        ln_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View menuItemView = getView().findViewById(R.id.ln_menu);
                PopupMenu popupMenu = new PopupMenu(getContext(),menuItemView);
                popupMenu.getMenuInflater().inflate(R.menu.menu_item_service,popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener (new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.sort_by_nearly:
                                txt_title_service.setText("Theo vị trí gần đây");
                                mServices.clear();
                                mServices.add(new Services(1,"Spood Spa","0.5 km","","-25%"));
                                mServices.add(new Services(2,"Night Spa","1.2 km","","-20%"));
                                mServices.add(new Services(3,"LODGE Spa","2.1 km", "","-40%"));
                                mServices.add(new Services(4,"Pet Spa","3 km" ,"","=-25"));
                                updateRCV();
                                break;
                            case R.id.sort_by_sale:
                                txt_title_service.setText("Theo giá khuyến mãi");
                                mServices.clear();
                                mServices.add(new Services(1,"Pet Spa","3 km" ,"","=-40%"));
                                mServices.add(new Services(2,"LODGE Spa","2.1 km", "","-35"));
                                mServices.add(new Services(3,"Night Spa","1.2 km","","-35%"));
                                mServices.add(new Services(4,"Spood Spa","0.5 km","","-30%"));
                                updateRCV();
                                break;
                                default: initData();
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
        return mView;
    }

    private void initView(){
        mRecyclerView = mView.findViewById(R.id.rcv_cat_servcice);
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        mRecyclerView.setLayoutManager(manager);
            ln_menu = (LinearLayout) mView.findViewById(R.id.ln_menu);
            txt_title_service=(TextView) mView.findViewById(R.id.txt_title_service);
    }

    private void initData(){
        mServices =  new ArrayList<>();
        mServices.add(new Services(1,"Spood Spa","0.5 km","Đánh giá :7.5/10","-25%"));
        mServices.add(new Services(2,"Night Spa","1.2 km","Đánh giá : 7/10","-20%"));
        mServices.add(new Services(3,"LODGE Spa","2.1 km", "Đánh giá 6.5/10","-40%"));
        mServices.add(new Services(4,"Pet Spa","3 km" ,"Đánh giá : 8.7/10","=-25"));
        updateRCV();

    }


    private void updateRCV(){
        if (mBeakServiceAdapter == null){
            mBeakServiceAdapter =  new BeakServiceAdapter(getContext(),mServices);
            mRecyclerView.setAdapter(mBeakServiceAdapter);
        }else{
            mBeakServiceAdapter.notifyDataSetChanged();
        }
    }

}
