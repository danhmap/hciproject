package com.example.projecthci.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.projecthci.R;
import com.example.projecthci.activities.Middle2Activity;
import com.example.projecthci.activities.MiddleActivity;
import com.example.projecthci.activities.SearchActivity;
import com.example.projecthci.activities.StoreNearlyActivity;
import com.example.projecthci.adapter.BannerAdapter;
import com.example.projecthci.adapter.ServiceHomeFragmentAdapter;

import java.util.ArrayList;
import java.util.List;


public class HomeFragment extends Fragment implements View.OnClickListener {

    private View mView;

    private ViewPager mVPBanner, mVPService;
    private ImageView mImgDog, mImgCat, mImgDogTrim , mImgDogBeak;
    private LinearLayout mLLDots, mLLButtonDog, mLLButtonCat , mLLservice1, mLLservice2;
    private ServiceHomeFragmentAdapter mServiceHomeFragmentAdapter;
    private List<Integer> mBanners;
    private TextView searchView;
    private BannerAdapter mBannerAdapter;
    private int mDotsCount;

    private ImageView[] dots;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        initView();
        initData();


        return mView;
    }

    private void initView() {
        mVPService = mView.findViewById(R.id.vp_service_homepage);
        mVPBanner = mView.findViewById(R.id.vp_banner_homepage);
        mLLDots = mView.findViewById(R.id.ll_dots);
        mImgCat = mView.findViewById(R.id.img_cat_service);
        mImgDog = mView.findViewById(R.id.img_dog_service);
        mImgDogBeak = mView.findViewById(R.id.img_dog_service1);
        mImgDogTrim = mView.findViewById(R.id.img_dog_service2);
        mLLButtonCat = mView.findViewById(R.id.ll_cat_servcie);
        mLLButtonDog = mView.findViewById(R.id.ll_dog_service);
        mLLservice1 = mView.findViewById(R.id.ll_dog_service1);
        mLLservice2 = mView.findViewById(R.id.ll_dog_service2);
        searchView=mView.findViewById(R.id.search_view);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initData() {
        mLLButtonDog.setOnClickListener(this);
        mLLButtonCat.setOnClickListener(this);
        mLLservice1.setOnClickListener(this);
        mLLservice2.setOnClickListener(this);

        mServiceHomeFragmentAdapter = new ServiceHomeFragmentAdapter(getChildFragmentManager());
        mVPService.setAdapter(mServiceHomeFragmentAdapter);
        mVPService.setCurrentItem(0);
        mVPService.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                if (position==0){
                    selectDogService();
                }else if (position==1){
                    selectService1();
                }else if (position==2){
                    selectService2();
                }else{
                    selectCatService();
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mBanners = new ArrayList<>();
        mBanners.add(R.drawable.banner11);
        mBanners.add(R.drawable.banner22);
        mBanners.add(R.drawable.banner33);
        mBanners.add(R.drawable.banner44);
        mBanners.add(R.drawable.banner55);

        createBanner();
    }

    private void selectDogService() {
        mLLButtonDog.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.buttom_home_selected));
        mLLButtonCat.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_home_unselect));
        mLLservice1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_home_unselect));
        mLLservice2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_home_unselect));
    }

    private void selectService1(){
        mLLButtonCat.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_home_unselect));
        mLLButtonDog.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_home_unselect));
        mLLservice1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.buttom_home_selected));
        mLLservice2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_home_unselect));
    }
    private void selectService2(){
        mLLButtonCat.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_home_unselect));
        mLLButtonDog.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_home_unselect));
        mLLservice1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_home_unselect));
        mLLservice2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.buttom_home_selected));
    }

    private void selectCatService() {
        mLLButtonDog.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_home_unselect));
        mLLservice1.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_home_unselect));
        mLLservice2.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_home_unselect));
        mLLButtonCat.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.buttom_home_selected));
    }


    private void createBanner(){
        mBannerAdapter = new BannerAdapter(getContext(), mBanners);
        mVPBanner.setAdapter(mBannerAdapter);
        mDotsCount = mBannerAdapter.getCount();
        dots = new ImageView[mDotsCount];
        for (int i = 0; i < mDotsCount; i++) {
            dots[i] = new ImageView(getContext());
            dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.ic_unselect_dot));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(8, 0, 8, 0);
            mLLDots.addView(dots[i], params);
        }
        dots[0].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.ic_slected_dot));
        mVPBanner.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < mDotsCount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(),R.drawable.ic_unselect_dot));
                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(),R.drawable.ic_slected_dot));
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_dog_service:
                mVPService.setCurrentItem(0);
                break;
            case R.id.ll_dog_service1:
                mVPService.setCurrentItem(1);
                break;
            case R.id.ll_dog_service2:
                mVPService.setCurrentItem(2);
                break;
            case R.id.ll_cat_servcie:
                mVPService.setCurrentItem(3);
                break;


        }
    }
}
