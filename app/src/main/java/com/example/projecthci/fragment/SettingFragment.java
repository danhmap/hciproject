package com.example.projecthci.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.projecthci.R;
import com.example.projecthci.activities.Booked;
import com.example.projecthci.activities.LoginActivity;
import com.example.projecthci.activities.Membership;
import com.example.projecthci.activities.PostActivity;
import com.example.projecthci.activities.SaleActivity;


public class SettingFragment extends Fragment implements View.OnClickListener {
    private Button mbtn_sale , mbtn_nearly , mbtn_booked , mbtn_logout, mbtn_post;
    private View mView;
    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView =  inflater.inflate(R.layout.fragment_setting, container, false);
        initView();
        initData();
        return mView;
    }

    private void initView() {
        mbtn_sale = (Button) mView.findViewById(R.id.btn_sale);
        mbtn_nearly = (Button) mView.findViewById(R.id.btn_nearly);
        mbtn_booked = (Button) mView.findViewById(R.id.btn_booked);
        mbtn_logout = (Button) mView.findViewById(R.id.btn_logout);
    }

    private void initData() {
        mbtn_logout.setOnClickListener(this);
        mbtn_nearly.setOnClickListener(this);
        mbtn_sale.setOnClickListener(this);
        mbtn_booked.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_logout:
                Intent intent = new Intent(getActivity() , LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_nearly:
                Intent intent1 = new Intent(getActivity(), Membership.class);
                startActivity(intent1);
                break;
            case R.id.btn_sale:
                Intent intent2 = new Intent(getActivity(), SaleActivity.class);
                startActivity(intent2);
                break;
            case R.id.btn_booked:
                Intent intent3 = new Intent(getActivity(), Booked.class);
                startActivity(intent3);
                break;

        }
    }
}
