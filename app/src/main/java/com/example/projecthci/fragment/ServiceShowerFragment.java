package com.example.projecthci.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.projecthci.R;
import com.example.projecthci.adapter.DogServiceAdapter;
import com.example.projecthci.models.Services;

import java.util.ArrayList;
import java.util.List;


public class ServiceShowerFragment extends Fragment {

    private View mView;
    private DogServiceAdapter mDogServiceAdapter;
    private List<Services> mServices;
    private RecyclerView mRecyclerView;
    private LinearLayout ln_menu;
    private TextView txt_title_service;

    public ServiceShowerFragment() {
        // Required empty public constructor
    }

    public static ServiceShowerFragment newInstance() {
        ServiceShowerFragment fragment = new ServiceShowerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_service_shower, container, false);
        initView();
        initData();
        ln_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View menuItemView = getView().findViewById(R.id.ln_menu);
                PopupMenu popupMenu = new PopupMenu(getContext(),menuItemView);
                popupMenu.getMenuInflater().inflate(R.menu.menu_item_service,popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.sort_by_nearly:
                                txt_title_service.setText("Theo vị trí gần đây ");
                                mServices.clear();
                                mServices.add(new Services(1,"Spood Spa","0.5 km","","-25%"));
                                mServices.add(new Services(2,"Night Spa","1.2 km","","-20%"));
                                mServices.add(new Services(3,"LODGE Spa","2.1 km", "","-40%"));
                                mServices.add(new Services(4,"Pet Spa","3 km" ,"","=-25"));
                                updateRCV();
                                break;
                            case R.id.sort_by_sale:
                                txt_title_service.setText("Theo giá khuyến mãi");
                                mServices.clear();
                                mServices.add(new Services(1,"Pet Spa","3 km" ,"","=-40%"));
                                mServices.add(new Services(2,"LODGE Spa","2.1 km", "","-35"));
                                mServices.add(new Services(3,"Night Spa","1.2 km","","-35%"));
                                mServices.add(new Services(4,"Spood Spa","0.5 km","","-30%"));
                                updateRCV();
                                break;
                            default: initData();
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
        return mView;
    }

    private void initView(){
        mRecyclerView = mView.findViewById(R.id.rcv_dog_servcice);
        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        mRecyclerView.setLayoutManager(manager);
        ln_menu = (LinearLayout) mView.findViewById(R.id.ln_menu);
        txt_title_service = (TextView) mView.findViewById(R.id.txt_title_service);

    }

    private void initData(){
        mServices =  new ArrayList<>();
        mServices.add(new Services(1,"Pet Spa ","0,7 km" ,"Đánh giá : 8.7/10","-30%"));
        mServices.add(new Services(2,"Spoocl Spa","2 km","Đánh giá :7.5/10","-25%"));
        mServices.add(new Services(3,"LODGE Spa","2,5 km","Đánh giá : 7/10","-25%"));
        mServices.add(new Services(4,"NINTH Spa","4,2 km", "Đánh giá 6.5/10","-20%"));
        updateRCV();

    }

    private void updateRCV(){
        if (mDogServiceAdapter == null){
            mDogServiceAdapter =  new DogServiceAdapter(getContext(),mServices);
            mRecyclerView.setAdapter(mDogServiceAdapter);
        }else{
            mDogServiceAdapter.notifyDataSetChanged();
        }
    }

}
